<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidos', function (Blueprint $table) {
            $table->unsignedBigInteger("id");
            $table->unsignedBigInteger("pais1_id");
            $table->unsignedBigInteger("pais2_is");
            $table->tinyInteger("goles_pais1");
            $table->tinyInteger("goles_pais2");
            $table->tinyInteger("disputado");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidos');
    }
}
