<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJugadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jugadores', function (Blueprint $table) {
            $table->unsignedBigInteger("id");
            $table->string("nombre");
            $table->tinyInteger("numeroCamiseta");
            $table->date("fechaNacimiento");
            $table->string("posicion");
            $table->unsignedBigInteger("pais_id")->references("id")->on("paises");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jugadores');
    }
}
