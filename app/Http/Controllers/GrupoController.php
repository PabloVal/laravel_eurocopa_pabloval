<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    public function index()
    {
        $arrayGrupos=Grupo::all();
        return view('grupos.index', ['arrayGrupos' =>$arrayGrupos]);
    }
}
