<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    use HasFactory;

    protected $table="grupos";
    protected $id;
    protected $letra;

    public function pais(){
        return $this->hasMany(Pais::class);
    }

}
