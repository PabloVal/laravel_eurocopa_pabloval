<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    use HasFactory;

    protected $table="jugadores";
    protected $id;
    protected $nombre;
    protected $numeroCamiseta;
    protected $posicion;
    protected $paisId;

    public function pais(){
        return $this->hasOne(Pais::class);
    }

}
