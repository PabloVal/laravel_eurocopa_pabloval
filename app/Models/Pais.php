<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;

    protected $table="paises";
    protected $id;
    protected $nombre;
    protected $slug;
    protected $grupo_id;



    public function grupos(){
        return $this->hasOne(Grupo::class);
    }

    public function partidos(){
        return $this->belongsToMany(Partido::class);
    }

}
