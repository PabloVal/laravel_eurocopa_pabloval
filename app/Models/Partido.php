<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    use HasFactory;


    protected $table="partidos";
    protected $id;
    protected $pais1_id;
    protected $pais2_id;
    protected $goles_pais1;
    protected $goles_pais2;
    protected $disputado;


    public function pais(){
        return $this->hasMany(Pais::class);
    }

}
