<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view("grupos.index");
})->name("/");


Route::get('grupos', function () {
    return view("grupos.index");
})->name("grupos.index");

Route::get('paises/{pais}', function () {
    return view("paises.pais");
})->name("grupos.index");

Route::get('partido/{partido}/disputar', function () {
    return view("grupos.index");
})->name("grupos.index");